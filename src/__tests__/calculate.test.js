import React from 'react';
import calculate from "../logic/calculate";


describe('Calculate Tests', () => {
    test('Number button tests', () => {
        expect(calculate({
            previous: null,
            current: null,
            operation: null
        }, "4")).toHaveProperty('current', "4");
        expect(calculate({
            previous: null,
            current: 1,
            operation: null
        }, "1")).toHaveProperty('current', "11");
        expect(calculate({
            previous: null,
            current: null,
            operation: "+"
        }, "1")).toHaveProperty('current', "1");
    });
    test('Equal button tests', () => {
        expect(calculate({
            previous: 2,
            current: 2,
            operation: "+"
        }, "=")).toHaveProperty('previous', "4");
        expect(calculate({
            previous: 2,
            current: 21,
            operation: "+"
        }, "=")).toHaveProperty('previous', "23");
    });
    test('Percentage button test', () => {
        expect(calculate({
            previous: 3,
            current: 8,
            operation: "+"
        }, "%")).toHaveProperty('previous', "0.11");
    });
    test('Percentage button test', () => {
        expect(calculate({
            previous: 12,
            current: 3,
            operation: "-"
        }, "%")).toHaveProperty('previous', "0.09");
    });
    test('Percentage button test', () => {
        expect(calculate({
            previous: 12,
            current: 6,
            operation: "-"
        }, "%")).toHaveProperty('previous', "0.06")
    });
    test('Percentage button test', () => {
        expect(calculate({
            previous: 4,
            current: 2,
            operation: "x"
        }, "%")).toHaveProperty('previous', "0.08");
    });

    test('Plus-Minus button tests', () => {
        expect(calculate({
                previous: null,
                current: 1,
                operation: null},"+/-")).toHaveProperty('current', "-1");

    });

    test('Plus-Minus button tests', () => {
        expect(calculate({
                previous: 8,
                current: null,
                operation: null},"+/-")).toHaveProperty('previous', "-8");

    });

    test('Plus-Minus button tests', () => {
        expect(calculate({
                previous: 8,
                current: 5,
                operation: null},"+/-")).toHaveProperty('current', "-5");

    });
    test('Plus-Minus button tests', () => {
        expect(calculate({
                previous: null,
                current: -3,
                operation: null},"+/-")).toHaveProperty('current', "3");

    });
    test('Plus-Minus button tests', () => {
        expect(calculate({
                previous: null,
                current: null,
                operation: null},"+/-")).toHaveProperty('[]', {});

    });

    test('Decimal button tests 1', () => {
        expect(calculate({
                previous: "2",
                current: "2",
                operation:null},".")).toHaveProperty('current', "2.");
        });
    test('Decimal button tests 2', () => {
        expect(calculate({
                previous: "8",
                current: "7",
                operation:null},".")).toHaveProperty('current', "7.");
        });
    test('Decimal button tests 3', () => {
        expect(calculate({
                previous: "5",
                current: "9",
                operation:null},".")).toHaveProperty('current', "9.");
        });
    test('Decimal button tests 4', () => {
        expect(calculate({
                previous: null,
                current: "6.8",
                operation:null},".")).toHaveProperty("[]",{});
        });

});
